package com.TVO203.TiendaVirtual.dao;

import com.TVO203.TiendaVirtual.model.Producto;
import org.springframework.data.repository.CrudRepository;


public interface ProductoDao extends CrudRepository<Producto,Integer>{
    
}
