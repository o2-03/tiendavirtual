package com.TVO203.TiendaVirtual.dao;

import com.TVO203.TiendaVirtual.model.Proveedor;
import org.springframework.data.repository.CrudRepository;


public interface ProveedorDao extends CrudRepository<Proveedor, Integer>{
    
}
