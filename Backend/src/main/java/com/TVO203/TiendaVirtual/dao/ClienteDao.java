package com.TVO203.TiendaVirtual.dao;

import com.TVO203.TiendaVirtual.model.Cliente;
import org.springframework.data.repository.CrudRepository;



public interface ClienteDao extends CrudRepository<Cliente, Integer>{
    
}
