package com.TVO203.TiendaVirtual.dao;

import com.TVO203.TiendaVirtual.model.Factura;
import org.springframework.data.repository.CrudRepository;

public interface FacturaDao extends CrudRepository<Factura, Integer>{
    
}
