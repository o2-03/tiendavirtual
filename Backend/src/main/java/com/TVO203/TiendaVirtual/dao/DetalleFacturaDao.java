package com.TVO203.TiendaVirtual.dao;

import com.TVO203.TiendaVirtual.model.DetalleFactura;
import org.springframework.data.repository.CrudRepository;


public interface DetalleFacturaDao extends CrudRepository<DetalleFactura, Integer>{
    
}
