package com.TVO203.TiendaVirtual.dao;

import com.TVO203.TiendaVirtual.model.Usuario;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


public interface UsuarioDao extends CrudRepository<Usuario,Integer>{
    @Transactional(readOnly=true)
    @Query(value="SELECT * FROM usuario WHERE activo = 1 and usuario = :usuario and clave = :clave", nativeQuery = true)
    public Usuario login(@Param("usuario") String usuario, @Param("clave") String clave);
}
