package com.TVO203.TiendaVirtual.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cliente")
public class Cliente implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idcliente")
    private Integer idcliente;
    
    @Column(name="nitcliente")
    private Integer nitcliente;
    
    @Column(name="nombrecliente")
    private String nombrecliente;
    
    @Column(name="celularcliente")
    private String celularcliente;
    
    @Column(name="direccioncliente")
    private String direccioncliente;

    public Integer getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Integer idcliente) {
        this.idcliente = idcliente;
    }

    public Integer getNitcliente() {
        return nitcliente;
    }

    public void setNitcliente(Integer nitcliente) {
        this.nitcliente = nitcliente;
    }

    public String getNombrecliente() {
        return nombrecliente;
    }

    public void setNombrecliente(String nombrecliente) {
        this.nombrecliente = nombrecliente;
    }

    public String getCelularcliente() {
        return celularcliente;
    }

    public void setCelularcliente(String celularcliente) {
        this.celularcliente = celularcliente;
    }

    public String getDireccioncliente() {
        return direccioncliente;
    }

    public void setDireccioncliente(String direccioncliente) {
        this.direccioncliente = direccioncliente;
    }

    

    
    
}
