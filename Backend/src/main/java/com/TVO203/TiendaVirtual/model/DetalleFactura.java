package com.TVO203.TiendaVirtual.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="detallefactura")
public class DetalleFactura implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="iddetalle")
    private Integer iddetalle;
    
    @ManyToOne
    @JoinColumn(name="idfactura")
    private Factura factura;
    
    @ManyToOne
    @JoinColumn(name="idproducto")
    private Producto producto;
    
    @Column(name="cantidadpedido")
    private Integer cantidadpedido;
    
    @Column(name="preciototal")
    private double preciototal;

    public Integer getIddetalle() {
        return iddetalle;
    }

    public void setIddetalle(Integer iddetalle) {
        this.iddetalle = iddetalle;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }


    public Integer getCantidadpedido() {
        return cantidadpedido;
    }

    public void setCantidadpedido(Integer cantidadpedido) {
        this.cantidadpedido = cantidadpedido;
    }

    public double getPreciototal() {
        return preciototal;
    }

    public void setPreciototal(double preciototal) {
        this.preciototal = preciototal;
    }
    
}
