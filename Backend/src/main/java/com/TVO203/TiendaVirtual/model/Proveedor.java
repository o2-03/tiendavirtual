package com.TVO203.TiendaVirtual.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="proveedor")
public class Proveedor implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idproveedor")
    private Integer idproveedor;
    
    @Column(name="nombreproveedor")
    private String nombreproveedor;
    
    @Column(name="celularproveedor")
    private String celularproveedor;
    
    @Column(name="direccionproveedor")
    private String direccionproveedor;

    public Integer getIdproveedor() {
        return idproveedor;
    }

    public void setIdproveedor(Integer idproveedor) {
        this.idproveedor = idproveedor;
    }

    public String getNombreproveedor() {
        return nombreproveedor;
    }

    public void setNombreproveedor(String nombreproveedor) {
        this.nombreproveedor = nombreproveedor;
    }

    public String getCelularproveedor() {
        return celularproveedor;
    }

    public void setCelularproveedor(String celularproveedor) {
        this.celularproveedor = celularproveedor;
    }

    public String getDireccionproveedor() {
        return direccionproveedor;
    }

    public void setDireccionproveedor(String direccionproveedor) {
        this.direccionproveedor = direccionproveedor;
    }
    
    
}
