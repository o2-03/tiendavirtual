package com.TVO203.TiendaVirtual.Controller;

import com.TVO203.TiendaVirtual.model.Cliente;
import com.TVO203.TiendaVirtual.service.ClienteService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("*")
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    private ClienteService clienteservice;
    
    @PostMapping(value="/")
    public ResponseEntity<Cliente> agregar(@RequestBody Cliente cliente){
        Cliente obj = clienteservice.save(cliente);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<Cliente> eliminar(@PathVariable Integer id){
        Cliente obj = clienteservice.findById(id);
        if(obj!=null)
            clienteservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        }
    
    @PutMapping(value="/")
    public ResponseEntity<Cliente> editar(@RequestBody Cliente cliente){
        Cliente obj = clienteservice.findById(cliente.getIdcliente());
        if(obj!=null){
            obj.setNitcliente(cliente.getIdcliente());
            obj.setNombrecliente(cliente.getNombrecliente());
            obj.setCelularcliente(cliente.getCelularcliente());
            obj.setDireccioncliente(cliente.getDireccioncliente());
            clienteservice.save(obj);
        }else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        }
    
    @GetMapping("/list")
    public List<Cliente> consultarTodo(){
        return clienteservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public Cliente consultaPorId(@PathVariable Integer id){
        return clienteservice.findById(id);
    }
}
