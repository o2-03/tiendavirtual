package com.TVO203.TiendaVirtual.Controller;

import com.TVO203.TiendaVirtual.model.DetalleFactura;
import com.TVO203.TiendaVirtual.service.DetalleFacturaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/detallefactura")
public class DetalleFacturaController {
    @Autowired
    private DetalleFacturaService detalleFacturaservice;
    
    @PostMapping(value="/add")
    public ResponseEntity<DetalleFactura> agregar(@RequestBody DetalleFactura detalleFactura){
        DetalleFactura obj = detalleFacturaservice.save(detalleFactura);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }
    
    @DeleteMapping(value="/{id}")
    public ResponseEntity<DetalleFactura> eliminar(@PathVariable Integer id){
        DetalleFactura obj = detalleFacturaservice.findById(id);
        if(obj!=null)
            detalleFacturaservice.delete(id);
        else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        }
    
    @PutMapping(value="/edit")
    public ResponseEntity<DetalleFactura> editar(@RequestBody DetalleFactura detalleFactura){
        DetalleFactura obj = detalleFacturaservice.findById(detalleFactura.getIddetalle());
        if(obj!=null){
            obj.setFactura(detalleFactura.getFactura());
            obj.setProducto(detalleFactura.getProducto());
            obj.setCantidadpedido(detalleFactura.getCantidadpedido());
            obj.setPreciototal(detalleFactura.getPreciototal());
            detalleFacturaservice.save(obj);
        }else
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(obj, HttpStatus.OK);
        }
    
    @GetMapping("/list")
    public List<DetalleFactura> consultarTodo(){
        return detalleFacturaservice.findAll();
    }
    
    @GetMapping("/list/{id}")
    public DetalleFactura consultaPorId(@PathVariable Integer id){
        return detalleFacturaservice.findById(id);
    }
}
