package com.TVO203.TiendaVirtual.service;

import com.TVO203.TiendaVirtual.model.Proveedor;
import java.util.List;


public interface ProveedorService {
    public Proveedor save(Proveedor proveedor);
    public void delete(Integer id);
    public Proveedor findById(Integer id);
    public List<Proveedor> findAll();
}
