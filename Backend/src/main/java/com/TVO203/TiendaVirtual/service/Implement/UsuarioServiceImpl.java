package com.TVO203.TiendaVirtual.service.Implement;

import com.TVO203.TiendaVirtual.dao.UsuarioDao;
import com.TVO203.TiendaVirtual.model.Usuario;
import com.TVO203.TiendaVirtual.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UsuarioServiceImpl implements UsuarioService{
    
    @Autowired
    private UsuarioDao usuarioDao;
    
    @Override
    @Transactional(readOnly=false)
    public Usuario save(Usuario usuario){
        return usuarioDao.save(usuario);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        usuarioDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public Usuario findById(Integer id){
        return usuarioDao.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Usuario> findAll(){
        return (List<Usuario>) usuarioDao.findAll();
    }
    
    @Override
    @Transactional(readOnly=true)
    public Usuario login(String userName, String password){
        return usuarioDao.login(userName, password);
    }
}
