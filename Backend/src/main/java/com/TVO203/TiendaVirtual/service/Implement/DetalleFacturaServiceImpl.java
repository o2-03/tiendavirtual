package com.TVO203.TiendaVirtual.service.Implement;

import com.TVO203.TiendaVirtual.dao.DetalleFacturaDao;
import com.TVO203.TiendaVirtual.model.DetalleFactura;
import com.TVO203.TiendaVirtual.service.DetalleFacturaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DetalleFacturaServiceImpl implements DetalleFacturaService{
    @Autowired
    private DetalleFacturaDao detalleFacturaDao;
    
    @Override
    @Transactional(readOnly=false)
    public DetalleFactura save(DetalleFactura detalleFactura){
    return detalleFacturaDao.save(detalleFactura);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
    detalleFacturaDao.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public DetalleFactura findById(Integer id){
    return detalleFacturaDao.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<DetalleFactura> findAll(){
    return (List<DetalleFactura>) detalleFacturaDao.findAll();
    }
    
}
