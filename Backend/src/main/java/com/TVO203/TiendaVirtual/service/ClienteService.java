package com.TVO203.TiendaVirtual.service;

import com.TVO203.TiendaVirtual.model.Cliente;
import java.util.List;

public interface ClienteService {
    public Cliente save(Cliente cliente);
    public void delete(Integer id);
    public Cliente findById(Integer id);
    public List<Cliente> findAll();
}
