package com.TVO203.TiendaVirtual.service;

import com.TVO203.TiendaVirtual.model.Usuario;
import java.util.List;


public interface UsuarioService {
    public Usuario save(Usuario usuario);
    public void delete(Integer id);
    public Usuario findById(Integer id);
    public List<Usuario> findAll();
    public Usuario login(String nombre, String clave);
}
