package com.TVO203.TiendaVirtual.service;

import com.TVO203.TiendaVirtual.model.Producto;
import java.util.List;

public interface ProductoService {
    public Producto save(Producto producto);
    public void delete(Integer id);
    public Producto findById(Integer id);
    public List<Producto> findAll();
}
