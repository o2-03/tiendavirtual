package com.TVO203.TiendaVirtual.service;

import com.TVO203.TiendaVirtual.model.Factura;
import java.util.List;


public interface FacturaService {
    public Factura save(Factura factura);
    public void delete(Integer id);
    public Factura findById(Integer id);
    public List<Factura> findAll();
}
