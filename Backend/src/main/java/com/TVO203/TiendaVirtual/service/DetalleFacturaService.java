package com.TVO203.TiendaVirtual.service;

import com.TVO203.TiendaVirtual.model.DetalleFactura;
import java.util.List;


public interface DetalleFacturaService {
    public DetalleFactura save(DetalleFactura detalleFactura);
    public void delete(Integer id);
    public DetalleFactura findById(Integer id);
    public List<DetalleFactura> findAll();
}
