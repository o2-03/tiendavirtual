
function loadData(){
    let request = sendRequest('cliente/list', 'GET', '')
    let table = document.getElementById('cliente-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idcliente}</th>
                    <td>${element.nitcliente}</td>
                    <td>${element.nombrecliente}</td>
                    <td>${element.celularcliente}</td>
                    <td>${element.direccioncliente}</td>
                    <td>
                        <div class="form-check form-switch">
                            <input ${element.activo ? "checked" : "uncheked"} class="form-check-input" type="checkbox" role="switch" disabled>
                        </div>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_Clientes.html?id=${element.idcliente}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteUser(${element.idcliente})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="5">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadCliente(idcliente){
    let request = sendRequest('cliente/list/'+idcliente, 'GET', '')
    let nit= document.getElementById('cliente-nit')
    let name = document.getElementById('cliente-name')
    let celular = document.getElementById('cliente-cel')
    let direccion = document.getElementById('cliente-dir')
    let id = document.getElementById('cliente-id')
    
    request.onload = function(){
        
        let data = request.response;
        nit.value = data.nitcliente
        name.value = data.nombrecliente
        celular.value = data.celularcliente
        direccion.value = data.direccioncliente
        id.value = data.idcliente
        
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteCliente(idcliente){
    let request = sendRequest('cliente/'+idcliente, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveCliente(){
    let nit = document.getElementById('cliente-nit').value
    let name = document.getElementById('cliente-name').value
    let celular = document.getElementById('cliente-cel').value
    let direccion = document.getElementById('cliente-dir').value
    let id = document.getElementById('cliente-id').value
    let data = {'idcliente': id,'nitcliente':nit,'nombrecliente':name ,'celularcliente':celular,'direccioncliente':direccion}
    let request = sendRequest('cliente/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'Clientes.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}