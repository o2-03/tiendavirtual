
function loadData(){
    let request = sendRequest('detallefactura/list', 'GET', '')
    let table = document.getElementById('detallefactura-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.iddetalle}</th>
                    <td>${element.idfactura}</td>
                    <td>${element.idproducto}</td>
                    <td>${element.cantidadpedido}</td>
                    <td>${element.preciototal}</td>
                    <td>
                        <div class="form-check form-switch">
                            <input ${element.activo ? "checked" : "uncheked"} class="form-check-input" type="checkbox" role="switch" disabled>
                        </div>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_detalle.html?id=${element.iddetalle}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteUser(${element.iddetalle})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="5">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadDetalle(iddetalle){
    let request = sendRequest('detallefactura/list/'+iddetalle, 'GET', '')
    let fact = document.getElementById('detalle-fact')
    let product = document.getElementById('detalle-prod')
    let cant = document.getElementById('detalle-cant')
    let precio = document.getElementById('detalle-precio')
    let id = document.getElementById('detalle-id')
    request.onload = function(){
        
        let data = request.response;
        fact.value = data.idfactura
        product.value = data.idproducto
        cant.value = data.cantidadpedido
        precio.value = data.preciototal
        id.value = data.iddetalle
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteDetalle(iddetalle){
    let request = sendRequest('detallefactura/'+iddetalle, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveDetalle(){
    let fact = document.getElementById('detalle-fact').value
    let product = document.getElementById('detalle-prod').value
    let cant = document.getElementById('detalle-cant').value
    let precio = document.getElementById('detalle-precio').value
    let id = document.getElementById('detalle-id').value
    let data = {'iddetalle': id,'idfactura':fact,'idproducto': product, 'cantidadpedido': cant, 'preciototal': precio}
    let request = sendRequest('detallefactura/add/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'detalle_facturas.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}