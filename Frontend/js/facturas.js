
function loadData(){
    let request = sendRequest('factura/list', 'GET', '')
    let table = document.getElementById('factura-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idfactura}</th>
                    <td>${element.fecha}</td>
                    <td>${element.idcliente}</td>
                    <td>${element.totalfactura}</td>
                    
                    <td>
                        <div class="form-check form-switch">
                            <input ${element.activo ? "checked" : "uncheked"} class="form-check-input" type="checkbox" role="switch" disabled>
                        </div>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_facturas.html?id=${element.idfactura}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteUser(${element.idfactura})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="5">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadFactura(idfactura){
    let request = sendRequest('factura/list/'+idfactura, 'GET', '')
    let fecha= document.getElementById('facturas-date')
    let cliente = document.getElementById('facturas-cliente')
    let totalfactura = document.getElementById('facturas-total')
  
    let id = document.getElementById('factura-id')
    
    request.onload = function(){
        
        let data = request.response;
        fecha.value = data.fecha
        cliente.value = data.idcliente
        totalfactura.value = data.totalfactura
        
        id.value = data.idfactura
        
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteFactura(idfactura){
    let request = sendRequest('factura/'+idfactura, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveFactura(){
    let fecha = document.getElementById('facturas-date').value
    let cliente= document.getElementById('facturas-cliente').value
    let totalfactura = document.getElementById('facturas-total').value
    let id = document.getElementById('factura-id').value
    let data = {'idfactura': id,'fecha':fecha,'idcliente':cliente ,'totalfactura':totalfactura}
    let request = sendRequest('factura/add/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'facturas.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}