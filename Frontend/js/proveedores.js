
function loadData(){
    let request = sendRequest('proveedor/list', 'GET', '')
    let table = document.getElementById('proveedor-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idproveedor}</th>
                    <td>${element.nombreproveedor}</td>
                    <td>${element.celularproveedor}</td>
                    <td>${element.direccionproveedor}</td>
                    <td>
                        <div class="form-check form-switch">
                            <input ${element.activo ? "checked" : "uncheked"} class="form-check-input" type="checkbox" role="switch" disabled>
                        </div>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_proveedores.html?id=${element.idproveedor}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteUser(${element.idproveedor})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="5">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadProveedor(idproveedor){
    let request = sendRequest('proveedor/list/'+idproveedor, 'GET', '')
    let name = document.getElementById('supplier-name')
    let celular = document.getElementById("supplier-cel")
    let direccion = document.getElementById('supplier-direction')
    let id = document.getElementById('supplier-id')
    
    request.onload = function(){
        
        let data = request.response;
        name.value = data.nombreproveedor
        celular.value = data.celularproveedor
        direccion.value = data.direccionproveedor
        id.value = data.idproveedor
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteProveedor(idproveedor){
    let request = sendRequest('proveedor/'+idproveedor, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveProveedor(){
    let name = document.getElementById('supplier-name').value
    let celular = document.getElementById('supplier-cel').value
    let direccion = document.getElementById('supplier-direction').value
    let id = document.getElementById('supplier-id').value
    let data = {'idproveedor': id,'nombreproveedor':name,'celularproveedor': celular,'direccionproveedor': direccion }
    let request = sendRequest('proveedor/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'proveedores.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}