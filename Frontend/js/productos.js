
function loadData(){
    let request = sendRequest('producto/list', 'GET', '')
    let table = document.getElementById('producto-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idproducto}</th>
                    <td>${element.nombreproducto}</td>
                    <td>${element.idproveedor}</td>
                    <td>${element.preciocosto}</td>
                    <td>${element.precioventa}</td>
                    <td>${element.cantidad}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_productos.html?id=${element.idproducto}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteProducto(${element.idproducto})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadProducto(idproducto){
    let request = sendRequest('producto/list/'+idproducto, 'GET', '')
    let name = document.getElementById('product-name')
    let supplier = document.getElementById('product-supplier')
    let purchase = document.getElementById('product-purchase')
    let sale = document.getElementById('product-sale')
    let quantity = document.getElementById('product-quantity')
    let id = document.getElementById('product-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.idproducto
        name.value = data.nombreproducto
        supplier.value = data.idproveedor
        purchase.value = data.preciocosto
        sale.value = data.precioventa
        quantity.value = data.cantidad

    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteProducto(idproducto){
    let request = sendRequest('producto/'+idproducto, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveProducto(){
    let name = document.getElementById('product-name').value
    let supplier = document.getElementById('product-supplier').value
    let purchase = document.getElementById('product-purchase').value
    let sale = document.getElementById('product-sale').value
    let quantity = document.getElementById('product-quantity').value
    let id = document.getElementById('product-id').value
    let data = {'idproducto': id,'nombreproducto':name,'idproveedor': supplier,'preciocosto': purchase, 'precioventa': sale, 'cantidad':quantity }
    let request = sendRequest('producto/add/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'productos.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}