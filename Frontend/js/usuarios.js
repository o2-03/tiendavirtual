
function loadData(){
    let request = sendRequest('usuario/list', 'GET', '')
    let table = document.getElementById('usuario-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.idusuario}</th>
                    <td>${element.usuario}</td>
                    <td>${element.clave}</td>
                    <td>
                        <div class="form-check form-switch">
                            <input ${element.activo ? "checked" : "uncheked"} class="form-check-input" type="checkbox" role="switch" disabled>
                        </div>
                    </td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_usuarios.html?id=${element.idusuario}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteUser(${element.idusuario})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="5">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadUsuario(idusuario){
    let request = sendRequest('usuario/list/'+idusuario, 'GET', '')
    let name = document.getElementById('user-name')
    let password = document.getElementById('user-password')
    let id = document.getElementById('user-id')
    let status = document.getElementById('user-status')
    request.onload = function(){
        
        let data = request.response;
        name.value = data.usuario
        password.value = data.clave
        id.value = data.idusuario
        status.checked = data.activo
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteUsuario(idusuario){
    let request = sendRequest('usuario/'+idusuario, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveUsuario(){
    let name = document.getElementById('user-name').value
    let password = document.getElementById('user-password').value
    let id = document.getElementById('user-id').value
    let status = document.getElementById('user-status').checked
    let data = {'idusuario': id,'usuario':name,'clave': password, 'activo': status }
    let request = sendRequest('usuario/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'usuarios.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}